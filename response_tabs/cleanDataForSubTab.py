import glob, os, sys
import pandas as pd
import datetime
import time

path = sys.argv[0]


def try_folder(filename):
    if filename.endswith('.txt'):
        base = os.path.splitext(path + '/' + filename)[0]
        os.rename(path + '/' + filename, base + ".csv")


# def convert_to_js_object(directory):
#     empty_list = []
#
#     for filename in glob.iglob(os.path.join(directory, 'aver.txt')):
#         col = ['time', 'value']
#         df = pd.read_csv(filename, delimiter='\t', names=col)
#         df = df.set_index('time')
#         count = 0
#         for index, row in df.iterrows():
#             my_string = str(index)
#             my_list = [index, row['value']]
#             empty_list.append(my_list)
#
#         for val in empty_list:
#             val[0] = int(time.mktime(datetime.datetime.strptime(str(val[0]), "%Y-%m-%d %H:%M:%S").timetuple()))
#         f = open(path + '/finalized.txt', 'w')
#         f.write(str(empty_list))
#         f.close()


def format_my_averages(df, filename):
    empty_list = []
    for index, row in df.iterrows():
        my_string = str(index)
        my_list = [index, row['value']]
        empty_list.append(my_list)
    for val in empty_list:
        val[0] = int(time.mktime(datetime.datetime.strptime(str(val[0]), "%Y-%m-%d %H:%M:%S").timetuple()))
    f = open(path + '/finalized' + str(filename) + '.txt', 'w')
    f.write(str(empty_list))
    f.close()


def find_monthly_average_text(filename):
    file_location = path + '/' + filename
    col = ['time', 'value']
    df = pd.read_csv(file_location, delimiter=',', names=col)
    df['time'] = pd.to_datetime(df['time'])
    df = df.set_index(['time'])
    return df.resample(str(43800) + 'T').mean()

if __name__ == "__main__":
    path = "/Users/luciaeve/Documents/dfki/RESULTS/01_Operational_Timeseries_Components"
    for filename in os.listdir(path):
        try_folder(filename)
        if filename.endswith(".csv"):
            my_averages = find_monthly_average_text(filename)
            format_my_averages(my_averages, filename)


